;global-parameters
(initialize-global-parameters (car *job-shop-problems*))
 *num-jobs*
 ;node-operations
 (create-initial-node)
 (action-valid-p (create-initial-node) (make-action :job-nr 0 :task-nr 0))
 (action-valid-p (create-initial-node) (make-action :job-nr 0 :task-nr 1))
 (action-valid-p (create-initial-node) (make-action :job-nr 1 :task-nr 0))
 (do-action (create-initial-node) (make-action :job-nr 0 :task-nr 0))
 (do-action (create-initial-node) (make-action :job-nr 1 :task-nr 0))
 (mapcar #'(lambda(x)(node-prev-action x)) (operators (create-initial-node)))
 (print-node-full-info (do-action (create-initial-node) (make-action :job-nr 0 :task-nr 0)))
 (print-node-full-info (do-action (do-action (create-initial-node) (make-action :job-nr 0 :task-nr 0)) make-action :job-nr 0 :task-nr 1))
 (print-node-full-info (car (operators (do-action (create-initial-node) (make-action :job-nr 0 :task-nr 0)))))
 
 (defparameter in-node (create-initial-node))
 (print-node-full-info (do-action (do-action in-node (make-action :job-nr 0 :task-nr 0)) (make-action :job-nr 0 :task-nr 1)))
 (print-node-full-info in-node)
 ;