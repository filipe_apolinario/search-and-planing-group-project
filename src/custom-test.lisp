(in-package :user)

(setf *custom-problems*
  (list
   (make-job-shop-problem
    :name "t1"
    :n.jobs 2
    :n.machines 2
    :jobs (list (MAKE-JOB-SHOP-JOB :JOB.NR 0
				   :TASKS (list (MAKE-JOB-SHOP-TASK :JOB.NR 0 
                                                                    :TASK.NR 0 
                                                                    :MACHINE.NR 0 
                                                                    :DURATION 2 
                                                                    :START.TIME NIL)
						(MAKE-JOB-SHOP-TASK :JOB.NR 0 
                                                                    :TASK.NR 1 
                                                                    :MACHINE.NR 0 
                                                                    :DURATION 3 
                                                                    :START.TIME NIL)
						(MAKE-JOB-SHOP-TASK :JOB.NR 0 
                                                                    :TASK.NR 2 
                                                                    :MACHINE.NR 0 
                                                                    :DURATION 4 
                                                                    :START.TIME NIL)))
                (MAKE-JOB-SHOP-JOB :JOB.NR 1
				   :TASKS (list (MAKE-JOB-SHOP-TASK :JOB.NR 1 
                                                                    :TASK.NR 0 
                                                                    :MACHINE.NR 1 
                                                                    :DURATION 4 
                                                                    :START.TIME NIL)
						(MAKE-JOB-SHOP-TASK :JOB.NR 1 
                                                                    :TASK.NR 1 
                                                                    :MACHINE.NR 0 
                                                                    :DURATION 2 
                                                                    :START.TIME NIL)
						(MAKE-JOB-SHOP-TASK :JOB.NR 1 
                                                                    :TASK.NR 2 
                                                                    :MACHINE.NR 1 
                                                                    :DURATION 1 
                                                                    :START.TIME NIL)))))))
