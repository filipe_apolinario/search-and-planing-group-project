;;;
;;; Copyright (C) Alexey Mazalov
;;;		  Filipe Apolin�rio <f.apolinario30@gmail.com> <Instituto Superior Tecnico - 70571
;;; 		  Maria Karpenko
;;;
;;; File: G012.lisp
;;; Created on: Mon April 27 2:13 2015

;Job-shop scheduling
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;DISCLAIMER:
;Sorry in advance to non-Portuguese speakers reading this code. 
;This code was written Portuguese to be consistent with the supporting material 'procura.lisp'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;CODE SUMMARY:
;code divided in 4 sections, IMPORT DE FICHEIROS AUXILIARES, DADOS AUXILIARES, ESTRUTURAS 
;and FUNCOES.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;DADOS AUXILIARES - global variables used in this file.

;ESTRUTURAS - Custom structures used in this file.

;FUNCOES - This section is declaration of functions in the project. 
;Also, this section is divided in the following subsections:
;
;		 - FUNCOES AUXILIARES SUPORTE
;		 - HEURISTICAS 		
;		 - FUNCAO "calendarizacao" - main function that solves a given problem with a 
;given strategy and returns the solution.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;IMPORT DE FICHEIROS AUXILIARES - imports supporting material.
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

(load (merge-pathnames "procura.lisp" *load-truename*))
;(load "C:\\onlyascii\\PPlan Proj Grupo\\search-and-planing-group-project\\src\\procura.lisp")

(load (merge-pathnames "job-shop-problemas-modelos.lisp" *load-truename*))
;(load "C:\\onlyascii\\PPlan Proj Grupo\\search-and-planing-group-project\\src\\job-shop-problemas-modelos.lisp")

(in-package :user)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;DADOS AUXILIARES
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defparameter *num-jobs* nil)

(defparameter *num-machines* nil)

(defparameter *max-num-tasks* nil)

(defparameter *job-task-matrix* nil)

(defparameter *max-run-time-seconds*  (* 1 30))       
;(defparameter *max-run-time-seconds* (* 5 60))

(defparameter *time-percentage-threshold* 0.98)

(defparameter *time-computation-start* nil)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;ESTRUTURAS 
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;an action a certain machine has to take
(defstruct action job-nr task-nr)

;node on the tree, has machine-time-array: -elapsed time on each machine until this node
                      ;constraint - structure constrain that has the information about 
	;all the task performed.
                      ;pre-action - action that leaded to this state (NOTE: this action 
	;is already on the constraints)
(defstruct node machine-time-array constraint prev-action) 

;constraint, has a matrix[jobs,tasks]->t/nil - stores the information about which actions
	;were performed until now.
                   ;array[jobs] - index of the last task performed in each job. (NOTE:This was
	;created to avoid the bottleneck of search for operators in the matrix,O(j*t), this way we 
	;know that the next task is index + 1 reducing the complexity to O(j)).
(defstruct constraint task-constraint-matrix task-constraint-index-array)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;
;;;FUNCOES
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;FUNCOES AUXILIARES SUPORTE
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;TIME
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun time-to-deliver-p ()
  (let ((elapsed-time  (/ (- (get-internal-run-time) 
                             *time-computation-start*) 
                          internal-time-units-per-second))) ;tempo de excucao do best-minimax
    (cond ((< elapsed-time (* *max-run-time-seconds* 
                              *time-percentage-threshold*))
           nil)
          (t
           ;(format t "~A" "running out of time") 
           t))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;GLOBAL INITIALIZERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;this method given a initializes all global parameters:
;	-variables given in problem: num-jobs,num-machines
;	-max-num-tasks, needed to initialize the matrix (columns).
;	-job-task-matrix
(defun initialize-global-parameters (problem)
	(let ((job-list (job-shop-problem-jobs problem)))
          (setf *time-computation-start* (get-internal-run-time))
          (setf *num-jobs* (job-shop-problem-n.jobs problem))
          (setf *num-machines* (job-shop-problem-n.machines problem))
          (setf *max-num-tasks* (fetch-max-num-tasks	job-list 
                                                        0))
          (setf *job-task-matrix* (make-array (list *num-jobs* 
                                                    *max-num-tasks*) 
                                              :initial-element nil))
		
          (initialize-matrix *job-task-matrix* job-list))) 



;given a list of jobs searched for job with the highest number of tasks
; and returns number of tasks in that job
(defun fetch-max-num-tasks (job-list 
                            max-tasks-value)
  (let ((job-list-copy (copy-list job-list)))
    (cond ((equal job-list ()) max-tasks-value)
          (t (max (length (job-shop-job-tasks (car job-list-copy))) 
                  max-tasks-value)))))



;inserts in the matrix the all tasks present in job list.
;matrix[job-nr, task-nr]->task
(defun initialize-matrix (matrix job-list)
	(let ((job-list-copy (copy-list job-list))
		  (job nil)
		  (task-list nil)
		  (task nil))
		;rows
		(loop for j-index 
                      from 0 to (- *num-jobs* 
                                   1) 
                      do (progn 
                           (setf job (car job-list-copy))
                           (setf task-list (copy-list (job-shop-job-tasks job)))
			;colums 
                           (loop for t-index 
                                 from 0 to (- *max-num-tasks* 
                                              1) 
                                 until (or (= t-index 
                                              *max-num-tasks*)
                                        (equal task-list 
                                               ())) 
                              do (progn 
                                   (setf task 
                                         (car task-list))
                                   (setf (aref matrix j-index t-index) 
                                         task)
                                   (setf task-list (cdr task-list))))
                           (setf job-list-copy (cdr job-list-copy))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;PROBLEM OPERATIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defun create-problema (heuristic)
  (make-problema :estado-inicial (create-initial-node)
                 :operadores (list #'operators)
                 :custo #'cost-function-v2
                 :heuristica heuristic
                 :hash #'sxhash
                 :objectivo? #'goal-p
                 :estado= #'equalp))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;NODE OPERATIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;creates the initial node, initializes: -> constraint-matrix positions to nil
;(nil represents that no task was performed
                                       ;->task-contraint-index-array with the first tasks
                                       ;-> machine-time-array to the initial elapsed type (magic number 0)
(defun create-initial-node ()
  (make-node :machine-time-array (make-array *num-machines* 
                                             :initial-element 0)
             :constraint (make-constraint :task-constraint-matrix (make-array (list *num-jobs* 
                                                                                    *max-num-tasks*) 
                                                                              :initial-element nil)
                                          :task-constraint-index-array (make-array *num-jobs* 
                                                                                   :initial-element 0))))



;creates an independent field copy of the node
;no action performed in the copy affect the original node 
(defun copy-node (node)
  (let ((machine-time-array-copy (copy-array (node-machine-time-array node)))
        (constraint-copy (make-constraint :task-constraint-matrix (copy-array (constraint-task-constraint-matrix (node-constraint node)))
                                          :task-constraint-index-array (copy-array (constraint-task-constraint-index-array (node-constraint node)))))
        (prev-action-copy nil))
    (when (not(null (node-prev-action node)))
      (setf prev-action-copy (make-action :job-nr (action-job-nr (node-prev-action node))
                                         :task-nr (action-task-nr (node-prev-action node)))))
    (make-node :machine-time-array machine-time-array-copy
               :constraint constraint-copy
               :prev-action prev-action-copy)))



;verifies that the given action is valid for the given node (state)
(defun action-valid-p (state task-action)
  (let ((constraint-matrix (constraint-task-constraint-matrix (node-constraint state))))
    (progn
      (when (and (equal (aref constraint-matrix 
                              (action-job-nr task-action) 
                              (action-task-nr task-action)) 
                        nil)
                 (or (equal (action-task-nr task-action)
                            0) 
                     (not (null (aref constraint-matrix 
                                  (action-job-nr task-action) 
                                  (- (action-task-nr task-action) 
                                     1)))))) 
        (return-from action-valid-p t))
      (return-from action-valid-p nil))))



;applies the action on the node (state)
;this method updates the following node fields:
;	-machine.time-array: updates the elapsed time where the task was executed adding the task's duration.
;	-constraint: updated the constraint matrix with the task executed and updates the array
;	-prev-action:updated to the action executed
;and returns the updated state or nil if the action cant be performed.
(defun do-action (state
                  task-action)
  (let ((state-copy (copy-node state)))
    (cond ((action-valid-p state-copy 
                           task-action) 
           (let* ((job-nr (action-job-nr task-action))
                  (task-nr (action-task-nr task-action))
                  (task (aref *job-task-matrix* 
                              job-nr
                              task-nr))
                  (target-machine (job-shop-task-machine.nr task))
                  (duration (job-shop-task-duration task))
                  (current-machine-elapsed (aref (node-machine-time-array state-copy)
                                                 target-machine))
                  (state-constraints (node-constraint state-copy)))
             (cond ((= task-nr 0)
                    (setf (aref (node-machine-time-array state-copy)
                                target-machine)
                          (+ current-machine-elapsed
                             duration))
                    (setf (aref (constraint-task-constraint-matrix state-constraints)
                                job-nr
                                task-nr) 
                          (+ current-machine-elapsed
                             duration)))
                   (t
                    (setf (aref (node-machine-time-array state-copy)
                                target-machine)
                          (+ (max current-machine-elapsed
                                  (aref (constraint-task-constraint-matrix state-constraints) 
                                        job-nr 
                                        (- task-nr 
                                           1)))
                             duration))
                    (setf (aref (constraint-task-constraint-matrix state-constraints)
                                job-nr
                                task-nr) 
                          (+ (max current-machine-elapsed
                                  (aref (constraint-task-constraint-matrix state-constraints) 
                                        job-nr 
                                        (- task-nr 
                                           1)))
                             duration))))
                   
             
             (setf (aref (constraint-task-constraint-index-array state-constraints)
                         job-nr)
                   (+ 1 
                      task-nr))
             (setf (node-prev-action state-copy) 
                   (make-action :job-nr job-nr 
                                :task-nr task-nr))
             (return-from do-action state-copy)))
          (t nil))))



;fetches all possible actions that can be performed by the parameter state.
;returns a list of all states achieved by all the possible actions.
(defun operators (state) 
  (let ((last-job-task-index-array (constraint-task-constraint-index-array (node-constraint state)))
        (sucessor-list ()))
    (loop for job-nr 
          from 0 
          to (- *num-jobs* 1) 
          do (let ((task-nr (aref last-job-task-index-array job-nr)))
               (cond ((time-to-deliver-p) ())
                     ((and (>= task-nr 0) 
                           (< task-nr *max-num-tasks*) 
                           (not (null (aref *job-task-matrix* job-nr task-nr))))
                      (setf sucessor-list (cons (do-action state (make-action :job-nr job-nr
                                                                              :task-nr task-nr))
                                                sucessor-list ))))))
    (return-from operators sucessor-list )))


;checks wether all tasks were successfuly executed
;if so returns true
;else returns false 
(defun goal-p (state)
  (progn
    (loop for job-index
        from 0
        to (- *num-jobs* 1)
        do (let ((task-index (aref (constraint-task-constraint-index-array (node-constraint state)) 
                               job-index)))
             (cond ((and (< task-index 
                           *max-num-tasks*)
                         (not (null (aref *job-task-matrix* 
                                          job-index 
                                          task-index)))) 
                    (return-from goal-p nil)))))
    t))



;checks returns the cost of performing node's prev-task
(defun cost-function-v1 (state)
  (job-shop-task-duration (aref *job-task-matrix* 
                                (action-job-nr (node-prev-action state))
                                (action-task-nr (node-prev-action state))))) 



;checks which machine had bigger execution time before executing the action
;compare time after executing task on the machine with the time on the previous machine executing time
;if the machine that executed the task exeds the previous machine bigger time
;return the difference between them
;else return  0.
(defun cost-function-v2 (state)
  (let* ((job-nr (action-job-nr (node-prev-action state)))
         (task-nr (action-task-nr (node-prev-action state)))
         (task (aref *job-task-matrix*
                    job-nr
                    task-nr))
         (targeted-machine (job-shop-task-machine.nr task))
         (duration (job-shop-task-duration task))
         (targeted-machine-elapsed-time (aref (node-machine-time-array state) 
                                              targeted-machine))
         (array-machines-before-op (copy-array (node-machine-time-array state))))
    (setf (aref array-machines-before-op 
                targeted-machine) 
          (- (aref array-machines-before-op targeted-machine) 
             duration))
    (let ((machine-max-time (car (machine-with-max-time array-machines-before-op))))
      (cond ((> targeted-machine-elapsed-time machine-max-time) 
             (- targeted-machine-elapsed-time machine-max-time))
            (t 0)))))
        

(defun machine-with-max-time (array)
  (let ((max-time 0)
        (max-time-machine-index 0))
    (loop for machine-index
          from 0
          to (- *num-machines* 1)
          do (cond ((< max-time (aref array machine-index))
                    (setf max-time (aref array machine-index))
                    (setf max-time-machine-index machine-index))))
    (cons max-time max-time-machine-index)))
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;DRAWING FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defun print-to-file (marker string)
  (with-open-file (str "C:\\onlyascii\\PPlan\ Proj\ Grupo\\search-and-planing-group-project\\results\\ILDS\\test-1\\teste.txt"
                       :direction :output
                       :if-exists :append
                       :if-does-not-exist :create)
    (cond ((null string)(format str marker)) 
          (t (format str marker string)))))
  
(defun print-job-task-matrix-detailed (tab)
  (loop for l from 0 to (- *num-jobs* 1) do
        (let((linha '()))
          (loop for c from 0 to (- *max-num-tasks* 1) do
                (setf linha 
                      (cons (aref tab 
                                  l 
                                  c) 
                            linha)))
          (format t "~A" (reverse linha))
          (format t "~%"))))

(defun print-job-task-matrix (tab)
  (loop for l 
        from 0 
        to (- *num-jobs* 1) 
        do (let((linha '()))
             (loop for c 
                   from 0 
                   to (- *max-num-tasks* 
                         1) 
                   do (setf linha 
                            (let ((task (aref tab 
                                              l 
                                              c)))  
                              (cons (cons (cons "m:" (job-shop-task-machine.nr task))
                                          (cons "d:" (job-shop-task-duration task))) 
                                    linha))))
             (format t "~A" (reverse linha))
             (format t "~%"))))


(defun print-node-full-info (state)
  (progn 
    (format t "~A" "##############")
    (format t "~%")
    (format t "~A" '(NODE INFORMATION))
    (format t "~%")
    (format t "~A" 'machine-time-array)
    (format t "~%")
    (format t "~A" (node-machine-time-array state))
    (format t "~%")
    (format t "~A" '(constraint))
    (format t "~%")
    (format t "~A" '(constraint-matrix))
    (format t "~%")
    (print-job-task-matrix (constraint-task-constraint-matrix (node-constraint state)))
    (format t "~%")
    (format t "~A" '(task-constraint-index-array))
    (format t "~%")
    (format t "~A" (constraint-task-constraint-index-array (node-constraint state)))
    (format t "~%")
    (format t "~A" '(prev-action))
    (format t "~%")
    (format t "~A" (node-prev-action state))
    (format t "~%")))



(defun print-node (state)
  (progn 
    (format t "~A" "##############")
    (format t "~%")
    (format t "~A" '(NODE INFORMATION))
    (format t "~%")
    (format t "~A" "##############")
    (format t "~%")
    (format t "~A" '(prev-action))
    (format t "~%")
    (if(not(null (node-prev-action state))) 
        (format t "~A" (node-prev-action state)))
    (format t "~%")
    (format t "~A" 'machine-time-array)
    (format t "~%")
    (if(not(null (node-machine-time-array state))) 
        (format t "~A" (node-machine-time-array state)))
    (format t "~%")
    (format t "~A" '(constraint-matrix))
    (format t "~%")
    (if(and (not(null (node-constraint state))) (not (null (constraint-task-constraint-matrix (node-constraint state)))))
        (format t "~A" (constraint-task-constraint-matrix (node-constraint state))))
    (format t "~%")))




(defun print-node-list (list detailed?)
  (cond ((null list) nil)
        (t (cond ((equal detailed? t)
                  (print-node-full-info (car list)))
                 ((equal detailed? nil)
                  (print-node (car list))))
           (print-node-list (cdr list) nil))))

(defun print-time-spent-computation ()
  (format t "~%")
    (format t "~A" "time spent on computation:")
    (format t "~%")
    (format t "~A" (/ (- (get-internal-run-time) *time-computation-start*) internal-time-units-per-second))
    (format t "~%"))

(defun print-procura-result (job-shop-result
                             detailed?)
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "Search Results:")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "##############")
  (format t "~%")
  (format t "~A" "Executing time:")
  (format t "~%")
  (format t "~A" (second job-shop-result))
  (format t "~%")
  (format t "~A" "Expanded nodes:")
  (format t "~%")
  (format t "~A" (third job-shop-result))
  (format t "~%")
  (format t "~A" "Generated nodes:")
  (format t "~%")
  (format t "~A" (fourth job-shop-result))
  (format t "~%")
  (format t "~A" "Path information:")
  (format t "~%")
  (print-node-list (first job-shop-result) detailed?)
  (format t "~%"))

(defun print-calendarizacao-result (task-list)
  (progn
    (loop for x 
          in  task-list
          do (progn 
               ;(format t "~%")
               (print-to-file "~%" nil)
               (print-task x)))
    ;(format t "~%")
    (print-to-file "~%" nil)
    (print-max-time task-list)))



(defun print-max-time (x) 
  (let ((time 0)) 
    ;(print "time needed to compute all tasks is:") 
    (print-to-file "~A" "time needed to compute all tasks is:") 
    (print-to-file "~%" nil) 
    
    (print-to-file "~A" (max-time-aux-recur time 
                               x))
    
    ;(print (max-time-aux-recur time 
    ;                           x))
    ;(format t "~%")
    (print-to-file "~%" nil)
    t))



(defun max-time-aux-recur (max-time x) 
  (when (equal () x) 
    (return-from max-time-aux-recur max-time))
  (let* ((task (car x)) 
         (task-time (+ (job-shop-task-duration task) 
                       (job-shop-task-start.time task)))) 
    (when (< max-time task-time) 
      (setf max-time task-time)) 
    (setf max-time (max-time-aux-recur max-time (cdr x) )) 
    (return-from max-time-aux-recur max-time)))


 
(defun print-task (task)
 ; (format t "~A" (list (cons "j:" (job-shop-task-job.nr task))
 ;                      (cons "t:" (job-shop-task-task.nr task))
 ;                      (cons "m:" (job-shop-task-machine.nr task))
 ;                      (cons "d:" (job-shop-task-duration task))
 ;                      (cons "s-time:" (job-shop-task-start.time task))))
  (print-to-file "~A" (list (cons "j:" (job-shop-task-job.nr task))
                            (cons "t:" (job-shop-task-task.nr task))
                            (cons "m:" (job-shop-task-machine.nr task))
                            (cons "d:" (job-shop-task-duration task))
                            (cons "s-time:" (job-shop-task-start.time task)))))
  




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;;;
;;;HEURISTICAS
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	

(defun dumb_heuristica (state)
  (let ((h 0))
  (loop for i from 0 to (- *num-jobs* 1) do 
         (loop for j from (aref (constraint-task-constraint-index-array (node-constraint state)) i) to (- *max-num-tasks* 1) do
               (if (not (eql (aref *job-task-matrix* i j) nil)) 
                   (setf h (+ h (job-shop-task-duration (aref *job-task-matrix* i j)))))))
  
    (return-from dumb_heuristica h)))

;***************************************
(defun rest_job_time_heuristica (state)
  (let ( (h 0) 
         (task (action-task-nr (node-prev-action state))) 
         (job (action-job-nr (node-prev-action state))))
    (loop for i from  (+ task 1) to (- *max-num-tasks* 1) do
          (progn  (if (eql (aref *job-task-matrix* job i) nil) 
                      (return-from rest_job_time_heuristica h) )
            (setf h 
                  (+ h 
                     (job-shop-task-duration (aref *job-task-matrix* 
                                                   job 
                                                   i))))))
    (return-from rest_job_time_heuristica h)))

;***************************************

 (defun min_maq_time_heuristica (state)
   (progn
     (when (null (node-prev-action state))
       (return-from min_maq_time_heuristica 99999999))
     (let ( (h 0) 
            (maq 0) 
            (task (action-task-nr (node-prev-action state))) 
            (job (action-job-nr (node-prev-action state))))
       (setf maq 
             (job-shop-task-machine.nr (aref *job-task-matrix* 
                                             job 
                                             task)))
       (loop for i 
             from 0 
             to (- *num-jobs* 
                    1) do (loop for j 
                               from (aref (constraint-task-constraint-index-array (node-constraint state)) 
                                          i) 
                               to (- *max-num-tasks* 
                                     1) 
                               do (if (and (eql maq 
                                                (job-shop-task-machine.nr (aref *job-task-matrix* 
                                                                                i 
                                                                                j))) 
                                           (not (eql (aref *job-task-matrix* 
                                                           i 
                                                           j) 
                                                     nil))) 
                                      (setf h 
                                            (+ h 
                                               (job-shop-task-duration (aref *job-task-matrix* 
                                                                             i 
                                                                             j)))))))
       
       (return-from min_maq_time_heuristica h))))

;***************************************

 (defun max_job_time_heuristica (state)
  (let ( (h 0) 
         (max 0))
    (loop for i 
          from 0 
          to (- *num-jobs* 1) 
          do  (progn
                (loop for j 
                      from (aref (constraint-task-constraint-index-array (node-constraint state)) i) 
                      to (- *max-num-tasks* 1) 
                      do (if (not (eql (aref *job-task-matrix* 
                                             i 
                                             j) 
                                       nil)) 
                          (setf h 
                                (+ h 
                                   (job-shop-task-duration (aref *job-task-matrix* 
                                                                 i 
                                                                 j))))))
                (if (< max 
                       h) 
                    (setf max 
                          h))
                (setf h 
                      0)))
    
    (return-from max_job_time_heuristica max)))

;***************************************

(defun collis_amount_heuristica (state)
  (let ((h 0)
        (max 999999999))
    (loop for i 
          from 0 
          to (- *num-jobs* 
                1) 
          do  (progn
                (loop for j 
                      from (aref (constraint-task-constraint-index-array (node-constraint state)) 
                                 i) 
                      to (- *max-num-tasks* 
                            1) 
                      do (if (not (eql (aref *job-task-matrix* 
                                             i 
                                             j) 
                                       nil)) 
                             (setf h (+ h 
                                        (job-shop-task-duration (aref *job-task-matrix* 
                                                                      i 
                                                                      j))))))
                (if (< max h) 
                    (setf max h))
                (setf h 0)))
    
    (return-from collis_amount_heuristica h)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;;;
;;;FUNCAO "calendarizacao"
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;DESCRIPTION:
;Function that produces a solution for each job in the given job-shop-problem.	
;	
;-Parameters:
;	problema: job-shop-problem data-type -defined in 
;		job-shop-problemas-modelos.lisp.
;	estrategia: string with the name of the strategy to be implemented.
;		Supported strategies:
;			"melhor.abordagem"
;			"a*.melhor.heuristica" - procura.lisp
;			"a*.melhor.heuristica.alternativa" - procura.lisp
;			"sondagem.iterativa" 
;			"ILDS"
;			"abordagem.alternativa"
;-Solution: list with all tasks needed to complete all problem's jobs.
;	Example:
;		(#S(JOB-SHOP-TASK :JOB.NR 0 :TASK.NR 0 :MACHINE.NR 1 :DURATION 12 :START.TIME 0) 
; 		#S(JOB-SHOP-TASK :JOB.NR 0 :TASK.NR 1 :MACHINE.NR 0 :DURATION 68 :START.TIME 12) 
; 		#S(JOB-SHOP-TASK :JOB.NR 1 :TASK.NR 0 :MACHINE.NR 0 :DURATION 5 :START.TIME 0) 
; 		#S(JOB-SHOP-TASK :JOB.NR 1 :TASK.NR 1 :MACHINE.NR 1 :DURATION 5 :START.TIME 12))
(defun calendarizacao (problema
                       estrategia)
  (let ((job-shop-result nil))
    (initialize-global-parameters problema)
    (cond	((equal estrategia 
                        "a*.melhor.heuristica") 
                 (setf job-shop-result 
                       (calendarizacao-a*-melhor-heuristica)))
                ((equal estrategia 
                        "a*.melhor.heuristica.alternativa") 
                 (setf job-shop-result 
                       (calendarizacao-a*-melhor-heuristica-alternativa)))
                ((equal estrategia 
                        "sondagem.iterativa") 
                 (setf job-shop-result 
                       (calendarizacao-sondagem-iterativa)))
                ((equal estrategia 
                        "ILDS") 
                 (setf job-shop-result 
                       (calendarizacao-ilds)))
                ((equal estrategia 
                        "abordagem.alternativa") 
                 (setf job-shop-result 
                       (calendarizacao-abordagem-alternativa)))
                ((equal estrategia 
                        "abordagem.alternativa2") 
                 (setf job-shop-result 
                       (calendarizacao-abordagem-alternativa2)))
                ((or (equal estrategia 
                            "profundidade")
                     (equal estrategia 
                            "profundidade-iterativa")
                     (equal estrategia 
                            "ida*")
                     (equal estrategia 
                            "largura"))
                 (setf job-shop-result 
                       (calendarizacao-abordagem estrategia))))

    ;(print-procura-result job-shop-result nil)
    ;(print-time-spent-computation)
    
    (prepare-calendarizacao-solution (first job-shop-result))))



;receives a solution-path and for each node in the path:
;calculates the start-up time if the task executed in the node (prev-action)
;updates the task start-up time
;and returns the updated tasks contained in the path solution.
(defun prepare-calendarizacao-solution (result-path)
  (cond ((null result-path) nil)
        ((null (node-prev-action (car result-path)))
         (prepare-calendarizacao-solution (cdr result-path)))
        (t (let* ((task-schedule ())
                  (state (car result-path))
                  (machine-array (node-machine-time-array state))
                  (task (aref *job-task-matrix*
                              (action-job-nr (node-prev-action state))
                              (action-task-nr (node-prev-action state)))))
             (setf (job-shop-task-start.time task)
                   (- (aref machine-array
                            (job-shop-task-machine.nr task))
                      (job-shop-task-duration task)))
             (setf task-schedule 
                   (list task))
             (let ((remaining-task-schedule (prepare-calendarizacao-solution (cdr result-path))))
               (cond((not (null remaining-task-schedule))
                     (nconc task-schedule
                            remaining-task-schedule))
                    (t (return-from prepare-calendarizacao-solution task-schedule))))))))

(defun calendarizacao-a*-melhor-heuristica ()
  (procura (create-problema #'collis_amount_heuristica) "a*"))

(defun calendarizacao-a*-melhor-heuristica-alternativa ()
  (procura (create-problema #'dumb_heuristica) "a*"))

(defun calendarizacao-sondagem-iterativa ()
  (procura-isp (create-problema #'dumb_heuristica)))

(defun calendarizacao-ILDS ()
  (procura-ilds (create-problema #'rest_job_time_heuristica)))

(defun calendarizacao-abordagem-alternativa ()
  (dds (create-problema #'dumb_heuristica)))

(defun calendarizacao-abordagem-alternativa2 ()
  (pppg (create-problema #'dumb_heuristica)))

(defun calendarizacao-abordagem (estrategia)
  (procura (create-problema #'dumb_heuristica) estrategia))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;;;
;;;FUNCTIONS Iterative Sampling Procedure
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;this search traverses the tree, selecting a sucessor at random until it reaches a leaf-node (objectivee node, in our problem representation).
;This algorithm first starts to see if it has time to compute annother path solution
;If it has time, computes a new solution and comapres to see if it is better than the previously found.
(defun procura-isp (problema)
  (let* ((best-path-time 99999999)
         (best-path nil)
         (temporary-path ())
         (node nil)
         (temporary-cost nil))
    (loop while (not(time-to-deliver-p))
          do 
          (progn
            ;(format t "~A" 
            (setf node (create-initial-node))
            (setf temporary-path ())
            (setf temporary-path (procura-isp-explore-node problema 
                                                           node 
                                                           temporary-path))
            (when (not (null temporary-path))
              (setf temporary-cost 
                    (car (machine-with-max-time (node-machine-time-array (car (last (first temporary-path)))))))
              (when (< temporary-cost best-path-time)
                (setf best-path-time temporary-cost)
                (setf best-path (first temporary-path))))))
    best-path))
  
;this function chooses, ramdomly, a sucessor for the received node. 
(defun procura-isp-sucessor (problema node)
  (let* ((successor-function (car (problema-operadores problema)))
         (successors (funcall successor-function
                             node)))
    (when (not (null successors))
      (nth (random (length successors))
                   successors))))

(defun procura-isp-explore-node (problema node path)
  (cond ((equal node nil) 
         (return-from procura-isp-explore-node nil))
        (t (setf path (nconc path (list node)))
           (cond ((funcall (problema-objectivo? problema) 
                           node) 
                  (return-from procura-isp-explore-node (list path (- (get-internal-run-time) *time-computation-start*) 0 0)))
                 (t (let* ((cdr-path (procura-isp-explore-node problema 
                                                               (procura-isp-sucessor problema
                                                                                     node)
                                                               path)))
                      (return-from procura-isp-explore-node cdr-path)))))))




  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;;;
;;;FUNCTIONS ILDS
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;this search traverses the tree fom the root node to the leaf node with a descrepancy criteria.

;First this algorithm, starts by traversing the tree with 0 maximum discrepancy (kmax). 
;This means that it follows the heuristic blindly until it reaches the root node (only best nodes are visited).
;Next the algorithm increments kmax by one unit and repeats the tree tranversal.
;The same process is computed until kmax = maximum depth.


;Discrepancy comes in to play when selecting the successors.
;Initialy k (node discrepancy) = kmax (maximum discrepancy).
;The sucessors are first ordered in a list by their heuristic value, better to worse.
;Then it is selected the first k sucessors.

;Imagine (discrepancy) k = 1, the sucessors 0 and 1.

;and it is decremented the k value by index of the sucessor chosen.
;The requirement of discrepancy is that in the leaf node the descrepancy be 0.
;Discrepancy k means that the algorithm can traverse the tree and select 0 to kth node by one unit

(defun procura-ilds (problema)
  (let* ((best-path-time 99999999)
         (best-path nil)
         (temporary-path ())
         (node nil)
         (temporary-cost nil)
         (descrepancy -1)
         (depth 0)
         (max-depth nil))

         (loop while (and (or (null max-depth) 
                              (and (not (null max-depth))
                                   (>= max-depth descrepancy)))
                          (not (time-to-deliver-p)))
               do 
               (progn
                 (setf node (create-initial-node))
                 (setf descrepancy (+ descrepancy 1))
                 (setf temporary-path ())
                 (setf temporary-path (procura-ilds-explore-node problema 
                                                                 node 
                                                                 temporary-path 
                                                                 descrepancy
                                                                 descrepancy
                                                                 depth
                                                                 max-depth))
                 (when (not (null temporary-path))
                   (setf max-depth (car temporary-path))
                   (setf temporary-cost 
                         (car (machine-with-max-time (node-machine-time-array (car (last (first (cdr temporary-path))))))))
                   (when (< temporary-cost best-path-time)
                     (setf best-path-time temporary-cost)
                     (setf best-path (cdr temporary-path))))))
    best-path))
  
(defun procura-ilds-sucessor (problema node)
  (let* ((successors (funcall (car (problema-operadores problema)) node))
         (heuristic-function  (problema-heuristica problema))
         (ordered-sucessors (mapcar #'(lambda(x)
                                        (cons (funcall heuristic-function x) 
                                              x))
                                    successors)))
    (sort ordered-sucessors #'> :key #'car)))

(defun procura-ilds-explore-node (problema 
                                  node 
                                  path  
                                  node-descrepancy
                                  max-descrepancy
                                  depth
                                  max-depth)
  (cond ((equal node 
                nil)
         (return-from procura-ilds-explore-node nil))
        (t (setf depth (+ depth 1))
           (setf path 
                 (nconc path 
                        (list node)))
           (cond ((funcall (problema-objectivo? problema) 
                           node) 
                  (return-from procura-ilds-explore-node (cons depth
                                                               (list path 
                                                                     (- (get-internal-run-time)
                                                                        *time-computation-start*) 
                                                                     0 
                                                                     0))))
                 (t
                  (let* ((sucessor-list (procura-ilds-sucessor problema
                                                                 node)))
                           (loop for index
                                 from 0
                                 to (length sucessor-list)
                                 do (cond ((> index node-descrepancy) (return-from procura-ilds-explore-node nil))

                                          ((or (null max-depth)
                                               (> ( - max-depth depth) max-descrepancy)
                                               (<= node-descrepancy (- max-depth depth)))
                                           (let ((cdr-path (procura-ilds-explore-node problema 
                                                                                      (cdr (nth index 
                                                                                                sucessor-list))
                                                                                      path
                                                                                      (- node-descrepancy 
                                                                                         index)
                                                                                      max-descrepancy
                                                                                      depth
                                                                                      max-depth)))
                                             (when (not (null cdr-path))
                                               (return-from procura-ilds-explore-node cdr-path))))))
                           (return-from procura-ilds-explore-node nil)))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;DDS
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun dds (problema)
	(let ((k 0) 
              (ret_val nil) 
              (depth 0)
              (timer(time-out #'dds-time-out)) 
              (estado (problema-estado-inicial problema))
              (ramification 1))
          (loop do
                (multiple-value-bind (aux res_depth ram) 
                    (dds-probe (make-no :pai nil 
                                        :estado estado) 
                               k 
                               1 
                               problema 
                               ramification) ;guarda o resultado da procura com k discrepancias
                  (setf  depth 
                         (max depth res_depth))
                  (setf ramification 
                        ram)
                  (when (and(not(null aux))
                            (or (null ret_val)
                                (melhor-que? (no-estado aux) 
                                             (no-estado ret_val)
                                             (problema-custo problema))));verifica qual a melhor solucao e guarda em ret_val
                    (setf ret_val aux)))
                (setf k(1+ k))
		while (and (not(time-to-deliver-p)) 
                           (<= k (* depth ramification)))); termina aquando verdadeira qualquer das 2 condicoes
          (transform-to-list ret_val)))




(defun dds-probe (estado 
                  iteracao 
                  profundidade 
                  problema 
                  ram)

	(if(funcall (problema-objectivo? problema) (no-estado estado))
            (values estado 
                    profundidade 
                    1)
          (let((filhos ())
               (op (problema-operadores problema)))
			;(print (cons iteracao profundidade))
            (dolist (operador op)
              (setf filhos 
                    (nconc filhos 
                           (funcall operador (no-estado estado)))))
            (sort filhos 
                  (compara-dois-a-dois (problema-heuristica problema)))
            (let ((n (length filhos)))
              (if(null filhos) (values nil profundidade 1)
                (cond ((<= iteracao 
                           (- profundidade 
                              1))
                       (multiple-value-bind(aux res_depth)
                           (dds-probe(make-no :pai estado 
                                              :estado (first filhos))
                                     iteracao 
                                     (1+ profundidade) 
                                     problema 
                                     ram)
                         (values aux res_depth (max (length filhos)
                                                    ram))))
                      ((and(< iteracao 
                              (+ profundidade 
                                 (- n 1)))
                           (> iteracao 
                              (- profundidade 
                                 1)))
                       (let((num (+ 1 (- iteracao profundidade))))
                         (multiple-value-bind(aux res_depth)
                             (dds-probe(make-no :pai estado 
                                                :estado (nth num filhos)) 
                                       (- iteracao 
                                          num) 
                                       (1+ profundidade) 
                                       problema 
                                       ram)
                           (values aux res_depth (max(length filhos)
                                                     ram)))))
                      (T(let((best nil)
                             (best_depth (1+ profundidade))
                             (n (length filhos)))
                          (dolist(filho filhos)
                            (multiple-value-bind(aux res_depth)
                                (dds-probe(make-no :pai estado 
                                                   :estado filho) 
                                          (- iteracao 
                                             (- n 
                                                1)) 
                                          (1+ profundidade) 
                                          problema 
                                          ram)
                              (if (and(not(null aux))
                                      (or (null best)
                                          (melhor-que? (no-estado aux) 
                                                       (no-estado best)
                                                       (problema-custo problema))));verifica qual a melhor solucao e guarda em ret_val
                                  (progn (setf best aux)
                                    (setf best_depth 
                                          res_depth))
                                (setf best_depth 
                                      (max best_depth res_depth)))))
                          (values best 
                                  best_depth 
                                  (max (length filhos)
                                       ram))))))))))

	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Fun�oes Auxiliares DDS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun melhor-que? (estado1 
                    estado2 
                    predicado)
	(< (funcall predicado estado1)
           (funcall predicado estado2)))
	
(defun compara-dois-a-dois (funcao)
	(lambda (estado1 estado2)
		(< (funcall funcao estado1) 
                   (funcall funcao estado2))))

(defun transform-to-list (no-final)
	(let((node no-final)
             (lista nil))
		(loop while(not(null node)) do
			(push (no-estado node) 
                              lista )
			(setf node 
                              (no-pai node)))
	(list lista)))
	
(defun soma(func1 func2)
	(lambda (arg)
		(+ (funcall func1 
                            arg)
                   (funcall func2 
                            arg))))

;funcao de time-out do DDS
(defun dds-time-out (actual_time last_iter start_time counter)
	(if (< counter 2)
		(> (/ (+ (- actual_time 
                            start_time) 
                         last_iter) 
                      internal-time-units-per-second) 
                   (* *max-run-time-seconds*
                      *time-percentage-threshold*));if true
		(> (/ (+ (- actual_time 
                            start_time) 
                         (* 2 
                            last_iter)) 
                      internal-time-units-per-second) 
                   (* *max-run-time-seconds*
                      *time-percentage-threshold*))));else 



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Procura em Profundidade Primeiro Guiada
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun pppg (problema)
  (let ((abertos nil)
        (timer (create-timer))
        (best nil))
    (setf abertos (list (make-no :pai nil 
                                 :estado (problema-estado-inicial problema))))
    (loop while(and(not(null abertos))
                   (not (time-to-deliver-p))) do 
          (when (funcall(problema-objectivo? problema)
                        (no-estado (first abertos)))
            (when (or(null best)
                     (melhor-que? (no-estado (first abertos))
                                  (no-estado best)
                                  (problema-custo problema)))
              (setf best (first abertos))))
          (let((estado (pop abertos))
               (filhos nil)
               (nos-filhos nil))
            (dolist(operador (problema-operadores problema))
              (setf filhos(nconc filhos (funcall operador 
                                                 (no-estado estado)))))
            (sort filhos (compara-dois-a-dois (soma (problema-heuristica problema)
                                                    (problema-custo problema))))
            (dolist (filho filhos)
              (setf nos-filhos (nconc (list(make-no :pai estado 
                                                    :estado filho)) 
                                      nos-filhos)))
            (setf abertos (nconc nos-filhos 
                                 abertos))))
    (transform-to-list best)))


(defun time-out (func)
	(let ((start_time (get-internal-run-time))
              (actual_time (get-internal-run-time))
              (last_iter 0)
              (counter 0))
		(lambda ()
			(setf counter(1+ counter))
			(setf last_iter (- (get-internal-run-time) actual_time))
			(setf actual_time (get-internal-run-time))
			(funcall func actual_time last_iter start_time counter))))


;funcao de time-out da PPPG
(defun create-timer ()
	(let((start-time (get-internal-run-time)))
		(lambda ()
			(let((actual-time (get-internal-run-time)))
				(< (/ (- actual-time start-time) internal-time-units-per-second) (* *max-run-time-seconds*
                                                                                                    *time-percentage-threshold*))))))
