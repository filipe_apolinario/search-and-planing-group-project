(defun dumb_heuristica (state)
  (let ((h 0))
  (loop for i from 0 to (- *num-jobs* 1) do 
         (loop for j from (aref (constraint-task-constraint-index-array (node-constraint state)) i) to (- *max-num-tasks* 1) do
               (if (not (eql (aref *job-task-matrix* i j) nil)) 
                   (setf h (+ h (job-shop-task-duration (aref *job-task-matrix* i j)))))))
  
    (return-from dumb_heuristica h)))

;***************************************
(defun rest_job_time_heuristica (state)
  (let ( (h 0) (task (action-task-nr (node-prev-action state))) (job (action-job-nr (node-prev-action state))))
        (loop for i from  (+ task 1) to (- *max-num-tasks* 1) do
              (progn  (if (eql (aref *job-task-matrix* job i) nil) (return-from earliest_job_heuristica h) )
                       (setf h (+ h (job-shop-task-duration (aref *job-task-matrix* job i))))
                        ))
        
    (return-from rest_job_time_heuristica h)))

;***************************************

 (defun min_maq_time_heuristica (state)
  (let ( (h 0) (maq 0) (task (action-task-nr (node-prev-action state))) (job (action-job-nr (node-prev-action state))))
    (setf maq (job-shop-task-machine.nr (aref *job-task-matrix* job task)))
    (loop for i from 0 to (- *num-jobs* 1) do 
         (loop for j from (aref (constraint-task-constraint-index-array (node-constraint state)) i) to (- *max-num-tasks* 1) do
               (if (and (eql maq (job-shop-task-machine.nr (aref *job-task-matrix* i j))) (not (eql (aref *job-task-matrix* i j) nil))) 
                   (setf h (+ h (job-shop-task-duration (aref *job-task-matrix* i j)))))))
        
    (return-from min_maq_time_heuristica h)))

;***************************************

 (defun max_job_time_heuristica (state)
  (let ( (h 0) (max 0))
    (loop for i from 0 to (- *num-jobs* 1) do 
          (progn
            (loop for j from (aref (constraint-task-constraint-index-array (node-constraint state)) i) to (- *max-num-tasks* 1) do
               (if (not (eql (aref *job-task-matrix* i j) nil)) 
                   (setf h (+ h (job-shop-task-duration (aref *job-task-matrix* i j))))))
            (if (< max h) (setf max h))
            (setf h 0)))
        
    (return-from max_job_time_heuristica max)))

;***************************************

(defun collis_amount_heuristica (state)
  (let ((h 0) () (task (action-task-nr (node-prev-action state))) (job (action-job-nr (node-prev-action state))))
    (loop for i from 0 to (- *num-jobs* 1) do 
       (progn
            (loop for j from (aref (constraint-task-constraint-index-array (node-constraint state)) i) to (- *max-num-tasks* 1) do
               (if (not (eql (aref *job-task-matrix* i j) nil)) 
                   (setf h (+ h (job-shop-task-duration (aref *job-task-matrix* i j))))))
            (if (< max h) (setf max h))
            (setf h 0)))
        
    (return-from collis_amount_heuristica h)))
